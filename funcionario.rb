class Funcionario
    attr_accessor :primeiro_nome, :segundo_nome, :idade, :salario, :endereco, :dependentes, :setor, :matricula
    
    def initialize(params)
        @primeiro_nome = params[:primeiro_nome]
        @segundo_nome = params[:segundo_nome]
        @idade = params[:idade]
        @salario = params[:salario]
        @endereco = params[:endereco]
        @dependentes = params[:dependentes]
        @setor = params[:setor]
        @matricula = params[:matricula]
    end
end
