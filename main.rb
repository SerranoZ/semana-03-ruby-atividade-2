require './bd.rb'
require 'faker'

projeto = Projeto.new({
    nome: 'Legal Bem Bacana',
    fundos: 10.000,
    nome_cliente: Faker::Name.first_name,
    departamento: 'IC UFF'
})

def cadastra_funcionario (projeto)
    Bd.adiciona_projetos "projetos", projeto
    Bd.adiciona_departamentos "departamentos", projeto.departamento
    puts "Departamento #{projeto.departamento} INFORMA:"
    puts "O(a) projeto #{projeto.nome} do cliente #{projeto.nome_cliente} foi cadastrado com sucesso!" 
end
cadastra_funcionario(projeto)



funcionario = Funcionario.new({
    primeiro_nome: Faker::Name.first_name, 
    segundo_nome: Faker::Name.middle_name,
    idade: Faker::Number.between(from: 20, to: 50),
    salario: 3.000,
    endereco: Faker::Address.street_address,
    dependentes: Dependente.new({
       nome: Faker::Name.first_name,
       idade: Faker::Number.between(from: 20, to: 50),
       parentesco: 'parente'
    }),
    setor: Faker::Appliance.brand,
    matricula: Faker::Number.number(digits: 4)
})

def cadastra_funcionario (funcionario)
    Bd.adiciona_funcionarios "funcionarios", funcionario
    puts "O(a) #{funcionario.primeiro_nome} #{funcionario.segundo_nome} foi cadastrado com sucesso!" 
    Bd.adiciona_dependentes "dependentes", funcionario.dependentes
    puts "O(a) #{funcionario.dependentes.nome}, parente do #{funcionario.primeiro_nome} foi cadastrado com sucesso!" 
end
cadastra_funcionario(funcionario)


